import numpy as np
import networkx as nx
from matplotlib import pyplot as plt
from mod_analysis import data_plot
import random
from tqdm import tqdm
import itertools


class Discussion:
    RELAXATION_CUTOFF = 0.0000001

    def __init__(self, oo, k):
        self.oo = oo  # opinions
        self.k = k  # coupling = discussion intensity

        self.a = 1  # attention

    @classmethod
    def init_discussion(cls, number, k, initial_split):
        oo = (np.random.rand(number)-initial_split)*4
        disc = Discussion(oo, k)
        disc.relax_individuals()
        return disc

    def relax_individuals(self):
        '''relax the individuals internal dynamics with no interactions'''
        k = self.k
        self.k = 0
        do_max = 10
        while do_max > Discussion.RELAXATION_CUTOFF:
            do_max = self.euler_update()
        self.k = k

    def get_polarization(self):
        '''computes the polarization of opinions'''
        return np.std(self.oo)

    def euler_update(self, eps=0.01):
        i = np.average(self.oo)
        dodt = -self.oo**3 + self.a*self.oo + self.k*i
        doo = eps*dodt
        self.oo += doo
        return max(doo)

    def evolve(self, time=None, eps=0.01, animate=True):
        for i in range(int(time/eps)) if time is not None else itertools.count():
            doo_max = self.euler_update(eps=eps)
            if animate:
                plt.gcf().clf()
                self.draw()
                plt.title(f"t={i*eps:.2f}, p={self.get_polarization():.2f}")
                plt.pause(0.01)
            if doo_max < Discussion.RELAXATION_CUTOFF:
                break

    def draw(self):
        plt.hist(self.oo, bins=99, range=(-4, 4))
        plt.show()

    def __len__(self):
        return len(self.oo)


def order_param(k, animate=False):
    disc = Discussion.init_discussion(number, k, initial_split=initial_split)
    # disc.relax_brains()
    disc.evolve(time=time, eps=0.01, animate=animate)
    return disc.get_polarization()


# def slide_control(kk, animate=False):
#     disc = Discussion.init_discussion(number, size, 0)
#     disc.relax_brains()
#     oo = []
#     for k in tqdm(kk):
#         disc.k = k
#         disc.evolve(time=time, eps=0.05, animate=animate)
#         o = disc.order_param()
#         oo.append(o)
#     return oo, disc


number = 1000
initial_split = 0.2
time = None


# disc = Discussion.init_discussion(number=number, k=0.47, initial_split=initial_split)
# disc.evolve(time, eps=0.01, animate=False)
# print(disc.get_polarization())

kk = np.linspace(0, 1, 100)
pp = []
for k in tqdm(kk):
    p = order_param(k, animate=False)
    pp.append(p)

plt.plot(kk, pp)

print(2/(6-9*initial_split))
