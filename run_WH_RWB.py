from mod_RWB import Landscape, Evolution, Venue, Discussion
from mod_NG import NetworkGenerator as NG
from mod_LP import LandscapePlot as LP
from mod_WH import WorkHorse
from mod_analysis import data_plot
from matplotlib import pyplot as plt
from random import random
import numpy as np
import scipy as sp
import sys
import time


# Check if we are running on the cluster or on the local machine
job = None
num_jobs = 1
min_job = 0  # starting index of the jobs
if len(sys.argv) > 1:
    running_on_cluster = True
    job = int(sys.argv[1])-1
    num_jobs = int(sys.argv[2])
else:
    running_on_cluster = False


class RWB_wrapper:

    possible_net_types = ['rand_reg', 'er', 'sw', 'sf']
    net_type = 'rand_reg'
    n = 100  # num of nodes
    # ------------------------------------------- rand reg net
    d = 3  # network degree
    # ------------------------------------------- er OR sw net
    q = 0.05  # edge probability OR rewiring probability respectively
    # ------------------------------------------- sw net
    K = 2  # 2K - degree before rewiring
    # ------------------------------------------- scale-free
    gamma = 3.5

    possible_ls_types = ['untargeted', 'targeted']
    ls_type = 'untargeted'
    # ------------------------------------------- discrete indep OR targeted k
    p = 0.1  # probability of k1 as opposed to k2
    k1 = -0.1
    k2 = 0.1

    N = 1000  # number of oscillator agents
    initial_split = 0.1  # percent of people with contrary opinion in the beginning
    dt = 0.001  # KM evolution time per iteration

    T = None  # total running time

    def __init__(self, params={}):
        self.set_params(params)
        self.construct_ls()
        self.populate_with_brains()
        self.make_evolution_object()

    def set_params(self, params):
        for k in params:
            val = int(params[k]) if int(params[k]) == params[k] else params[k]
            setattr(self, k, val)

    def construct_ls(self):
        if self.net_type == 'rand_reg':
            net = NG.random_regular(self.n, self.d).net
        elif self.net_type == 'er':
            net = NG.er(self.n, self.q).net
        elif self.net_type == 'sf':
            net = NG.my_scale_free(self.n, self.gamma).net
        elif self.net_type == 'sw':
            net = NG.small_world(self.n, 2*self.K, self.q).net

        self.ls = Landscape(net, self.N, self.initial_split)
        if self.ls_type == 'untargeted':
            self.ls.add_venues_by_kk_pp([self.k1, self.k2], [self.p, 1-self.p])
        elif self.ls_type == 'targeted':
            self.ls.add_venues_targeted(self.k1, self.k2, self.p)

    def populate_with_brains(self):
        self.ls.populate_venues_randomly()

    def make_evolution_object(self):
        self.ev = Evolution(self.ls, self.dt)
        self.ev.frames_per_iter = 1

    def run_till_relaxation(self, **args):
        self.ev.run_till_relaxation(**args)

    def run(self, verbose):
        if self.T is None:
            self.ev.run_till_relaxation(base_iters=1000, verbose=verbose)
        else:
            self.ev.run(iters=self.T, verbose=verbose)

    def get_order_param(self):
        return self.ev.rr[-1]

    def get_all_properties(self):
        props = {'net.typ.': self.net_type, 'ls.typ.': self.ls_type}
        if self.net_type == 'rand_reg':
            props.update({'n': self.n, 'd': self.d})
        elif self.net_type == 'er':
            props.update({'n': self.n, 'q': self.q})
        elif self.net_type == 'sf':
            props.update({'n': self.n, 'gamma': self.gamma})
        elif self.net_type == 'sw':
            props.update({'n': self.n, 'K': self.K, 'q': self.q})

        if self.ls_type == 'untargeted' or self.ls_type == 'targeted':
            props.update({'p': self.p, 'k1': self.k1, 'k2': self.k2})

        props.update({'N': self.N, 'dt': self.dt,
                      'initial_split': self.initial_split})
        return props

    def __str__(self):
        params = self.get_all_properties()
        # for key in params:
        #     if key=='dw': key='dw'
        name = ' '.join(([f'{k}={v}' for k, v in params.items()]))
        return 'RWO '+name


def f(param, save=False, verbose=False):
    rrwb = RWB_wrapper(param)
    if verbose:
        print(f' {rrwb}\n')
        sys.stdout.flush()
    rrwb.run(verbose=verbose)
    return rrwb.get_order_param()


def construct_WH(params, copies, name_prefix=''):
    print('WorkHorse construction started')
    wh = WorkHorse(f=f, params=params, iters=copies,
                   parallel=running_on_cluster, job=job,
                   num_jobs=num_jobs, min_job=min_job)
    wh.f_out_num = 1
    wh.first_in_its_kind = job == 0
    wh.ylabels = ['r']

    # set name
    print('guineapig construction started')
    guineapig = RWB_wrapper(params[0])
    properties = guineapig.get_all_properties()
    for key in params[0]:
        properties[key] = 'var.'
    wh.name = ' '.join(([f'{k}={v}' for k, v in properties.items()]))
    wh.name = name_prefix+wh.name
    return wh


def run_WH(wh, save=False):
    print('started computing')
    wh.compute_data()
    print('done computing')
    if running_on_cluster or save:
        print('saving')
        wh.save()
    return wh


def plot_wh_2d(wh, fig=None, flip_xy=False):
    var_names = list(wh.pp[0].keys())
    var_names.sort(reverse=flip_xy)
    x_name, y_name = var_names
    xx = []
    yy = []
    zz = []
    z_err = []
    if not hasattr(wh, 'means'):
        wh.compute_means_and_errors()
    for params, z, sd in zip(wh.pp, wh.means, wh.errors):
        xx.append(params[x_name])
        yy.append(params[y_name])
        zz.append(z)
        z_err.append(sd)

    if fig is None:
        fig = plt.figure()

    zmin = 0
    zmax = 1
    plt.figure(fig)
    data_plot.contour_plot(xx, yy, zz,
                           lvls=np.linspace(zmin*1.001, zmax*1.001, 100),
                           extend_up_down=[zmax < 1, zmin > 0],
                           ticks=[zmin, (zmin+zmax)/2, zmax],
                           labels=[x_name, y_name, 'r'],
                           colors='summer')


def get_pc_exp(wh, x, x_name, plot=False, copy=None):
    # for a given k, pick out data for r vs p from wh, and fit
    ppc = []
    for copy in range(len(wh.data[-1])) if copy == None else [copy]:
        pp = set()
        for params in wh.pp:
            if params[x_name] == x:
                pp.update({params['p']})
        pp = list(pp)
        pp.sort()
        rr = np.array([0.]*len(pp))
        # outliers = []
        for i, params in enumerate(wh.pp):
            if x == params[x_name]:
                index = np.where(pp == params['p'])[0][0]
                if copy == -1:
                    r = wh.means[i]
                else:
                    r = wh.data[i][copy]
                rr[index] = r
                # if np.abs(r-wh.means[i])>0.5:
                #     outliers.append(index)

        if plot:
            plt.plot(pp, rr, '.')
            ppp = np.linspace(0, 1, 1000)
            # plt.plot(ppp, r_approx(ppp, pc, a))
            plt.xlim([min(pp), max(pp)])

        # print("\n", list(rr))
        thresh = 0.3
        i = np.where(rr > thresh)[0][0]
        print((pp[i-1]*(rr[i]-thresh)+pp[i]*(thresh-rr[i-1])/(rr[i]-rr[i-1])))

        ppc.append(pp[i])

    return np.mean(ppc), sp.stats.sem(ppc)


def plot_pc_vs_x_exp(wh, x_name, **args):
    xx = set()
    for params in wh.pp:
        xx.update({params[x_name]})
    xx = list(xx)
    xx.sort()
    yy = []
    err = []
    for x in xx:
        y, er = get_pc_exp(wh, x, x_name)
        yy.append(y)
        err.append(er)
    if 'label' not in args:
        args['label'] = 'exp.'
    print(xx)
    print(yy)
    print(err)
    err = 0
    plt.errorbar(xx, yy, err, fmt='o', markersize=4, capsize=4, **args)
    plt.ylim(0, 1)


def pc_theo(x, name):
    guineapig = RWB_wrapper()
    prms = guineapig.get_all_properties()
    prms[name] = x
    if prms['net.typ.'] == 'rand_reg' and prms['ls.typ.'] == 'untargeted':
        n, k1, k2 = prms['n'], prms['k1'], prms['k2']
        N, initial_split = prms['N'], prms['initial_split']
        return (k2 - 2*n/(6-9*initial_split)/N) / (k2-k1)


def plot_pc_theo_vs_x(x0, x1, x_name):
    xx = np.linspace(x0, x1, 1000)
    yy = pc_theo(xx, x_name)
    if not hasattr(yy, '__iter__'):
        yy = [yy]*len(xx)
    # print(xx, yy)
    plt.plot(xx, yy, label='theo.')


print('python file launched')


x_name = 'k1'
x_min = RWB_wrapper.k1
x_max = -0.
x_num = 10

y_name = 'p'
del_y = 0.1
y_num = 20

params = []
for x in np.linspace(x_min, x_max, x_num):
    yc = pc_theo(x, x_name)
    del_y = min(del_y, 1-yc, yc)

    for y in np.linspace(yc-del_y/2, yc+del_y/2, y_num):
        params.append({y_name: y, x_name: x})


if running_on_cluster:
    wh = construct_WH(params, copies=10, name_prefix='RWB_WH')
    run_WH(wh, save=True)

# plot_wh_2d(wh)
# plot_pc_theo_vs_x(x_min, x_max, x_name)


else:
    wh_name = 'WH RWB_WHnet.typ.=rand_reg ls.typ.=untargeted n=100 d=3 p=var. k1=var. k2=0.1 N=1000 dt=0.001 initial_split=0.1'
    wh = WorkHorse.load(wh_name)
    # plot_pc_vs_x_exp(wh, x_name, label=wh_name)

    # plot_wh_2d(wh, flip_xy=False)
    plot_pc_theo_vs_x(x_min, x_max, x_name)
    plot_pc_vs_x_exp(wh, x_name)
    # for x in np.linspace(x_min, x_max, x_num):
    #     get_pc_exp(wh, x, x_name, plot=True)

    plt.title(wh, wrap=True)
    plt.xlabel(x_name)
    plt.ylabel(y_name)

    plt.xlabel("$k_-$")
    plt.ylabel("$p_c$")

    # plt.figure()
    # for i in range(10):
    #     # plt.clf()
    #     get_pc_exp(wh, 2.8, x_name, plot=True, copy=i)
    #     plt.pause(2)

    # get_kc_exp(wh, 2.5, 'gamma', plot=True, copy=-1)
    # ng = NG.scale_free_static_model(1000,4)
    # ng.plot_degree_distr_log(line=4)
