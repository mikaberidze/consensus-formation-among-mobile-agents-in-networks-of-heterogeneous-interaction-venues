#!/bin/sh
#SBATCH --job-name=rand_wlk_osc
#SBATCH --mail-type=END,FAIL        # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=mikaberidze@ucdavis.edu     # Where to send mail

#SBATCH --output=Out/rwo_%A-%a.log
#SBATCH --partition=parallel# bigmemh#  
##SBATCH --time=1-00:00:00

#SBATCH --cpus-per-task=32
##SBATCH --mem-per-cpu=500

#SBATCH --exclude=c2-75

#SBATCH --array=1-16       # Array range



python3 run_WH_RWB.py $SLURM_ARRAY_TASK_ID 16

