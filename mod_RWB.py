from mod_NG import NetworkGenerator as NG
from mod_LP import LandscapePlot as LP
from common import Saveable
from matplotlib import pyplot as plt
from scipy.integrate import solve_ivp
from tqdm import tqdm
import networkx as nx
import random
import numpy as np
import sys
import itertools


class Discussion:
    RELAXATION_CUTOFF = 0.0000001

    def __init__(self, oo, k, involved=None):
        self.oo = oo  # opinions
        self.k = k  # coupling = discussion intensity

        self.a = 1  # attention

        if involved is None:
            involved = np.ones(len(oo))
        self.involved = involved

    @classmethod
    def init_discussion(cls, number, k, initial_split):
        oo = np.ones(number)
        oo[:int(number*initial_split)] = -1

        disc = Discussion(oo, k)
        disc.relax_individuals()
        return disc

    def relax_individuals(self):
        '''relax the individuals internal dynamics with no interactions'''
        k = self.k
        self.k = 0
        do_max = 10
        while do_max > Discussion.RELAXATION_CUTOFF:
            do_max = self.euler_update()
        self.k = k

    def get_polarization(self):
        '''computes the polarization of opinions'''
        return np.std(self.oo)

    def euler_update(self, eps=0.01):
        oo = self.oo*self.involved
        i = np.sum(oo)
        dodt = -oo**3 + self.a*oo + self.k*i*self.involved
        doo = eps*dodt
        self.oo += doo
        return max(doo)

    def evolve(self, time=None, eps=0.01, animate=True):
        for i in range(int(time/eps)) if time is not None else itertools.count():
            doo_max = self.euler_update(eps=eps)
            if animate:
                self.draw()
            if doo_max < Discussion.RELAXATION_CUTOFF:
                break

    def draw(self):
        plt.gca().cla()
        plt.hist(self.oo, bins=99, range=(-4, 4))
        plt.title(f"p={self.get_polarization():.2f}")
        plt.show()
        plt.pause(0.01)

    def __len__(self):
        return len(self.oo)

# Venue with specific coupling, associated with a single node in
# the network and containing a set of iscillators


class Venue:
    def __init__(self, k, brains=None, N=None):
        if brains is None:
            brains = np.zeros(N)
        self.brains = brains
        self.k = k

    def add_brain(self, brain_id):
        self.brains[brain_id] = 1

    def remove_brain(self, brain_id):
        self.brains[brain_id] = 0

    def add_brains(self, brain_ids):
        for brain_id in brain_ids:
            self.add_brain(brain_id)

    def __str__(self):
        return str(np.where(self.brains == 1)[0])


# Network containing Venue
class Landscape(Saveable):
    def __init__(self, net, N, initial_split):
        self.net = net
        self.N = N
        self.nodes = sorted(list(net.nodes()))

        self.discussion = Discussion.init_discussion(N, 0, initial_split)
        self.discussion.relax_individuals()

    def add_venues(self, kk):
        self.venues = {self.nodes[i]: Venue(kk[i], N=self.N)
                       for i in range(len(self.net))}

    def add_venues_by_kk_pp(self, kk, pp):
        n = len(self.nodes)
        kkk = [k for (k, p) in zip(kk, pp) for i in range(int(n*p))]
        while len(kkk) != n:
            if len(kkk) > n:
                kkk.pop()
            else:
                kkk.append(kkk[-1])
        random.shuffle(kkk)
        self.add_venues(kkk)

    def add_venues_targeted(self, k1, k2, p):
        n = len(self.nodes)

        nd_deg = sorted(self.net.degree, key=lambda x: x[1]+0.1*random.random())
        nodelist = [nd for (nd, deg) in nd_deg]
        kkk = [k2]*n
        for nd in nodelist[int(n*(1-p)):]:
            kkk[nd] = k1
        self.add_venues(kkk)

    def populate_venues_randomly(self):
        for brain in self.get_all_brains():
            nd = random.choice(self.nodes)
            self.venues[nd].add_brain(brain)

    def get_all_brains(self):
        return list(range(self.N))

    def get_order_param(self):
        return self.discussion.get_polarization()

    @staticmethod
    def landscape_from_deg_cpl(dd, kk):  # dd is approximate degrees
        net = NG.configuration_model(dd).net
        ls = Landscape(net)
        ls.add_venues(kk)
        return ls

    def __str__(self):
        st = ""
        for venue in self.venues.values():
            st += "\n" + str(venue)
        return st


class Evolution(Saveable):
    MAX_ROUNDS = 30

    def __init__(self, ls, dt):
        self.ls = ls
        self.dt = dt
        self.frames_per_iter = 1  # number of
        self.monitor_r = True
        self.rr = []
        self.tt = []

    def random_walk_iteration(self, return_map=False):
        moved = set()
        oo_map = {}
        for nd in self.ls.nodes:
            neighbors = list(self.ls.net[nd])
            tomove = np.where(self.ls.venues[nd].brains == 1)[0]
            for o in tomove:
                if o not in moved:
                    new_nd = random.choice(neighbors)
                    self.ls.venues[nd].remove_brain(o)
                    self.ls.venues[new_nd].add_brain(o)
                    moved.add(o)
                    if return_map:
                        oo_map[o] = (nd, new_nd)
        assert len(moved) == self.ls.N, "not all brains moved"
        if return_map:
            return oo_map

    def euler_single(self, del_t, animate=False):
        for nd in self.ls.nodes:
            venue = self.ls.venues[nd]
            self.ls.discussion.k = venue.k
            self.ls.discussion.involved = venue.brains
            self.ls.discussion.euler_update(eps=del_t)
        if animate:
            self.ls.discussion.draw()

    def run(self, iters, verbose=False, animate=False):
        show_every_nth_frame = 100
        for j in (tqdm(range(iters)) if verbose else range(iters)):
            # if j < 2:
            #     print(self.ls)
            self.random_walk_iteration()
            for i in range(self.frames_per_iter):
                draw = animate and j % show_every_nth_frame == 0
                self.euler_single(self.dt/self.frames_per_iter, draw)
                if self.monitor_r:
                    self.record_r()

    def run_till_relaxation(self, base_iters=None, verbose=False, animate=False):
        if base_iters is None:
            base_iters = int(30/self.dt)

        rounds = 0
        while True:

            # if verbose and animate:
            plt.subplot(1, 2, 1)
            relaxed = self.check_if_relaxed(show=verbose) or rounds == self.MAX_ROUNDS
            if relaxed:
                if verbose:
                    print('system relaxed' if rounds <
                          self.MAX_ROUNDS else " max rounds reached")
                    sys.stdout.flush()
                break
            if verbose and len(self.tt) > 0:
                print(f'system is not relaxed, launching next '
                      f'{base_iters} iterations')
                sys.stdout.flush()

            # if verbose and animate:
            plt.subplot(1, 2, 2)
            self.run(base_iters, verbose=verbose, animate=animate)
            rounds += 1

    def record_r(self):
        t = 0 if len(self.tt) == 0 else self.tt[-1]
        t += self.dt/self.frames_per_iter
        r = self.ls.get_order_param()
        self.rr.append(r)
        self.tt.append(t)

    def check_if_relaxed(self, show=False):  # heuristic, not exact
        testpoints = 7  # split second half of data into 5 bins
        require_grater = 2  # demand that at least this many are > last val
        require_lesser = 2  # demand that at least this many are < last val
        tolerance = 0.003
        l = len(self.rr)
        last = int(l/3)
        if l < 3:
            return False
        if show:
            plt.plot(self.rr)
        return max(self.rr[-last:])-min(self.rr[-last:]) < tolerance or self.rr[-1] < 0.01

    def plot_r_vs_t(self, lbl=None):
        plt.plot(self.tt, self.rr, label=lbl)
        plt.xlabel('t')
        plt.ylabel('r')
        plt.legend()
        plt.pause(0.1)


if __name__ == "__main__":
    d = 3
    n = 100
    N = 1000
    initial_split = 0.1
    dt = 0.001

    kk = [0.1, -0.1]
    p = 0.31
    pp = [1-p, p]

    net = nx.random_regular_graph(d, n)
    ls = Landscape(net, N, initial_split)
    ls.add_venues_by_kk_pp(kk, pp)
    ls.populate_venues_randomly()

    ev = Evolution(ls, dt)
    ev.run_till_relaxation(1000, verbose=1, animate=1)
    # ev.run(100000, verbose=1, animate=1)

    # plt.figure()
    # ev.plot_r_vs_t()
