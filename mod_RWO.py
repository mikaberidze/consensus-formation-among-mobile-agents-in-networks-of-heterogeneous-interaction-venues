from mod_NG import NetworkGenerator as NG
from mod_LP import LandscapePlot as LP
from common import Saveable
from matplotlib import pyplot as plt
from scipy.integrate import solve_ivp
from tqdm import tqdm
import networkx as nx
import random
import numpy as np
import copy
import sys


# oscillators that will randomly walk on the network and sync / desync
class Oscillator:
    def __init__(self, phase=None, freq=0):
        self.phase = phase
        self.freq = freq
        self.color = None
        
        if self.phase==None:
            self.unif_phase()
    
    def unif_phase(self, phi_max=2*np.pi):
        self.phase = random.random()*phi_max
    
    def __str__(self):
        return f'oscillator φ={self.phase} ω={self.freq}'
    
    @staticmethod #generate N oscillators with normally distributed freqs
    def sample_normal_freq(N, w0, delw):
        return [Oscillator(freq=w) for w in np.random.normal(w0, delw, N)]


# Kuramoto platform with specific coupling, associated with a single node in 
# the network and containing a set of iscillators
class Kuramoto:
    def __init__(self, k, oscillators=set()):
        self.oscillators = set(oscillators)
        self.k = k
    
    def add_oscillator(self, osc):
        self.oscillators.add(osc)
    
    def remove_oscillator(self, osc):
        self.oscillators.remove(osc)
        
    def add_oscillators(self, oscs):
        self.oscillators.update(oscs)


# Network containing Kuramotos that contain 
class Landscape(Saveable):
    def __init__(self, net):
        self.net = net
        self.nodes = sorted(list(net.nodes()))
    
    def add_kms(self, kk):
        self.kms = {self.nodes[i]:Kuramoto(kk[i]) for i in range(len(self.net))}
    
    def add_kms_by_kk_pp(self, kk, pp):
        n = len(self.nodes)
        kkk = [k for (k,p) in zip(kk,pp) for i in range(int(n*p))]
        while len(kkk)!=n:
            if len(kkk)>n:
                kkk.pop()
            else:
                kkk.append(kkk[-1])
        random.shuffle(kkk)
        self.add_kms(kkk)
    
    def add_kms_targeted(self, k1, k2, p):
        n = len(self.nodes)
        
        nd_deg = sorted(self.net.degree, key=lambda x: x[1]+0.1*random.random())
        nodelist = [nd for (nd, deg) in nd_deg]
        kkk = [k2]*n
        for nd in nodelist[int(n*(1-p)):]:
            kkk[nd] = k1
        self.add_kms(kkk)
    
    def populate_kms_randomly(self, oscillators):
        for o in oscillators:
            nd = random.choice(self.nodes)
            self.kms[nd].add_oscillator(o)                    
        
    def get_all_oscillators(self):
        oscillators = set()
        for km in self.kms.values():
            oscillators.update(km.oscillators)
        return oscillators
    
    def get_order_param(self):
        phis = [o.phase for km in self.kms.values() for o in km.oscillators]

        xx = np.cos(phis)
        yy = np.sin(phis)

        return np.linalg.norm((np.mean(xx), np.mean(yy)))
    
    @staticmethod
    def landscape_from_deg_cpl(dd, kk): #dd is approximate degrees
        net = NG.configuration_model(dd).net
        ls = Landscape(net)
        ls.add_kms(kk)
        return ls


class Evolution(Saveable):
    def __init__(self, ls, dt, lp=None):
        self.ls = ls
        self.dt = dt
        self.lp = lp
        self.frames_per_iter = 1 #dt segmentation number for KM evolution
        self.monitor_r = True
        self.rr = []
        self.tt = []
        
        self.km_dynamics = self.km_dynamics_euler_single #self.km_dynamics_rk
    
    def random_walk_iteration(self, return_map=False):
        unmoved = self.ls.get_all_oscillators()
        oo_map = {}
        for nd in self.ls.nodes:
            neighbors = list(self.ls.net[nd])
            tomove = self.ls.kms[nd].oscillators.copy()
            for o in tomove:
                if o in unmoved:
                    new_nd = random.choice(neighbors)
                    self.ls.kms[nd].remove_oscillator(o)
                    self.ls.kms[new_nd].add_oscillator(o)
                    unmoved.remove(o)
                    if return_map:
                        oo_map[o] = (nd, new_nd)
        assert len(unmoved)==0, "not all oscillators moved"
        if return_map:
            return oo_map
    
    @staticmethod #Kuramoto equation: derivative of state vector

    def km_eqn(t, phi, ww, k):
        der_phi = np.zeros(len(phi))

        for i in range(len(phi)):
            der_phi[i] = ww[i]

        for i in range(len(phi)):
            for j in range(i,len(phi)):
                der_phi[i] += k*np.sin(phi[j]-phi[i])
                der_phi[j] += k*np.sin(phi[i]-phi[j])

        return der_phi
    
    def km_dynamics_rk(self, del_t, verbose=False):
        for nd in self.ls.nodes:
            km = self.ls.kms[nd]
            oo = list(km.oscillators)
            phi = [o.phase for o in oo]
            ww = [o.freq for o in oo]
            der = lambda t, phi: self.km_eqn(t, phi, ww, km.k)
            
            sol = solve_ivp(der, [0, del_t], phi)
        
            for i,o in enumerate(oo):
                o.phase = sol.y[i, -1]
    
    def km_dynamics_euler_single(self, del_t, verbose=False):
        for nd in self.ls.nodes:
            km = self.ls.kms[nd]
            oo = list(km.oscillators)
            phi = [o.phase for o in oo]
            ww = [o.freq for o in oo]
            
            phi += del_t * Evolution.km_eqn(0, phi, ww, km.k)
        
            for i,o in enumerate(oo):
                o.phase = phi[i]
                
            
    def run(self, iters, verbose=False):
        for j in (tqdm(range(iters)) if verbose else range(iters)):
            traj = self.random_walk_iteration(return_map = self.lp!=None)
            if self.lp!=None:
                self.lp.plot_2d_transition(traj)
            for i in range(self.frames_per_iter):
                self.km_dynamics(self.dt/self.frames_per_iter)
                if self.monitor_r:
                    self.record_r()
                if self.lp!=None:
                    self.lp.plot_2d()
                    plt.pause(0.01)
                        
    
    def run_till_relaxation(self, base_iters=None, verbose=False):
        if base_iters==None:
            base_iters = int( 3/self.dt )
        while True:
            relaxed = self.check_if_relaxed(show=verbose)
            if relaxed:
                if verbose:
                    print('system relaxed')
                    sys.stdout.flush()
                break
            if verbose and len(self.tt)>0:
                print(f'system is not relaxed, launching next '\
                      f'{base_iters} iterations')
                sys.stdout.flush()
            self.run(base_iters, verbose=verbose)
    
    def record_r(self):
        t = 0 if len(self.tt)==0 else self.tt[-1]
        t += self.dt/self.frames_per_iter
        r = self.ls.get_order_param()
        self.rr.append(r)
        self.tt.append(t)
        
    def check_if_relaxed(self, show=False): # heuristic, not exact
        testpoints = 7 # split second half of data into 5 bins
        require_grater = 2 # demand that at least this many are > last val
        require_lesser = 2 # demand that at least this many are < last val
        tolerance = 0.001
        l = len(self.rr)
        box = int(l/3/testpoints)
        if box==0:
            return False
        RR = [np.mean(self.rr[int(2*l/3+i*box) : int(2*l/3+(i+1)*box)])
                            for i in range(testpoints)]
        if show:
            TT = [np.mean(self.tt[int(2*l/3+i*box) : int(2*l/3+(i+1)*box)])
                                for i in range(testpoints)]
            plt.scatter(TT,RR)
            plt.pause(0.1)
        
        r_last = self.rr[-1]
        for R in RR:
            if R<=r_last+tolerance:
                require_lesser -= 1
            if R>=r_last-tolerance:
                require_grater -= 1
        
        return require_lesser<=0 and require_grater<=0
            
        
        
    def plot_r_vs_t(self, lbl=None):
        plt.plot(self.tt, self.rr, label=lbl)
        plt.xlabel('t')
        plt.ylabel('r')
        plt.legend()
        plt.pause(0.1)





