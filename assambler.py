#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 11:47:00 2020

@author: guga
"""

from mod_WH import WorkHorse
import sys


# Check if we are running on the cluster or on the local machine
n = None
if len(sys.argv)>2:
    running_on_cluster = True
    n = int(sys.argv[1])
    base = str(sys.argv[2])
else:
    running_on_cluster = False



WorkHorse.assamble(n, base)




