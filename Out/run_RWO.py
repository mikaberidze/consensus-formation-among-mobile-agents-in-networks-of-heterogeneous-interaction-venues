from mod_RWO import Landscape, Evolution, Kuramoto, Oscillator
from mod_NG import NetworkGenerator as NG
from mod_LP import LandscapePlot as LP
import numpy as np
from tqdm import tqdm
import time
import random
from matplotlib import pyplot as plt


class RWO_wrapper:

    possible_net_types = ['rand_reg', 'ER', 'sf']
    net_type = 'ER'
    n = 10 # num of nodes
    # ------------------------------------------- rand reg net
    d = 3 # network degree
    # ------------------------------------------- ER net
    q = 0.35 # edge probability
    # ------------------------------------------- scale-free
    gamma = 2.5

    possible_ls_types = ['disjoint', 'targeted']
    ls_type = 'disjoint'
    # ------------------------------------------- discrete indep or targeted k
    p = 0.2 # probability of k1 as opposed to k2
    k1 = -1
    k2 = 1
    
    
    
    N = 50 # number of oscillator agents
    w0 = 5 # mean frequency of oscillators
    dw = .2 # standard deviation of oscillator frequencies
    dt = 0.1 # KM evolution time per iteration
    
    def __init__(self, params={}):
        self.set_params(params)
        self.construct_ls()
        self.populate_with_oscillators()
        self.make_evolution_object()
    
    def set_params(self, params):
        for k in params:
            setattr(self, k, params[k])
    
    def construct_ls(self):
        if self.net_type == 'rand_reg':
            net = NG.random_regular(self.n, self.d).net
        elif self.net_type == 'ER':
            net = NG.er(self.n, self.q).net
        elif self.net_type == 'sf':
            net = NG.my_scale_free(self.n, self.gamma).net
            
        self.ls = Landscape(net)
        if self.ls_type == 'disjoint':
            self.ls.add_kms_by_kk_pp([self.k1,self.k2], [self.p,1-self.p])
        elif self.ls_type == 'targeted':
            self.ls.add_kms_targeted(self.k1, self.k2, self.p)
            
    def populate_with_oscillators(self):
        oo = Oscillator.sample_normal_freq(self.N,self.w0,self.dw)
        oo[-1].color = 'red'
        oo[-2].color = 'green'
        oo[-3].color = 'blue'
        oo[-4].color = 'orange'
        oo[-5].color = 'gray'
        oo[-6].color = 'yellow'
        oo[-7].color = 'black'
        self.ls.populate_kms_randomly(oo)
    
    def make_evolution_object(self):
        self.ev = Evolution(self.ls, self.dt)
        self.ev.frames_per_iter = 1
    
    def run_till_relaxation(self, **args):
        self.ev.run_till_relaxation(**args)
        
    def get_order_param(self):
        return self.ev.rr[-1]
    
    def get_all_properties(self):
        props = {'net.typ.':self.net_type, 'ls.typ.':self.ls_type}
        if self.net_type == 'rand_reg':
            props.update({'n':self.n, 'd':self.d})
        elif self.net_type == 'ER':
            props.update({'n':self.n, 'q':self.q})
        elif self.net_type == 'sf':
            props.update({'n':self.n, 'gamma':self.gamma})
        
        if self.ls_type == 'disjoint' or self.ls_type == 'targeted':
            props.update({'p':self.p, 'k1':self.k1, 'k2':self.k2})
            
        props.update({'N':self.N, 'dw':self.dw, 'dt':self.dt})
        return props
    
    def __str__(self):
        params = self.get_all_properties()
        # for key in params:
        #     if key=='dw': key='dw'
        name = ' '.join(([f'{k}={v}' for k,v in params.items()]))
        return 'RWO '+name




rwow = RWO_wrapper({})
lp = LP(rwow.ls)
lp.force_evolve_layout()
rwow.ev.lp = lp
lp.plot_2d()

# rwow.ev.frames_per_iter = 20
# rwow.ev.run(10, verbose=True)
# # rwow.run_till_relaxation(verbose=True)
# lp.export_animation('demo')

# rwow.ev.plot_r_vs_t('euler')





# random.seed(0)
# iters = 50
# dt = 0.1
# rwow = RWO_wrapper({'dt':dt})
# rwow.ev.km_dynamics = rwow.ev.km_dynamics_rk
# start = time.time()
# rwow.ev.run(iters, verbose=True)
# end = time.time()
# print(f'iters={iters} dt={dt} time={end-start}')
# rwow.ev.plot_r_vs_t('rk'












