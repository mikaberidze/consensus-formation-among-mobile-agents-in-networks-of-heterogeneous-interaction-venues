from mod_RWO import Landscape, Evolution, Kuramoto, Oscillator
from mod_NG import NetworkGenerator as NG
from mod_LP import LandscapePlot as LP
from mod_WH import WorkHorse
from mod_analysis import data_plot
from matplotlib import pyplot as plt
from random import random
import numpy as np
import scipy as sp
import sys
import time


# Check if we are running on the cluster or on the local machine
job = None
num_jobs = 1
min_job = 0  # starting index of the jobs
if len(sys.argv) > 1:
    running_on_cluster = True
    job = int(sys.argv[1])-1
    num_jobs = int(sys.argv[2])
else:
    running_on_cluster = False


class RWO_wrapper:

    possible_net_types = ['rand_reg', 'er', 'sw', 'sf']
    net_type = 'sf'
    n = 2000  # num of nodes
    # ------------------------------------------- rand reg net
    d = 3  # network degree
    # ------------------------------------------- er OR sw net
    q = 0.05  # edge probability OR rewiring probability respectively
    # ------------------------------------------- sw net
    K = 2  # 2K - degree before rewiring
    # ------------------------------------------- scale-free
    gamma = 3.5

    possible_ls_types = ['untargeted', 'targeted']
    ls_type = 'untargeted'
    # ------------------------------------------- discrete indep OR targeted k
    p = 0.1  # probability of k1 as opposed to k2
    k1 = -1
    k2 = 1

    N = 10000  # number of oscillator agents
    w0 = 0  # mean frequency of oscillators
    dw = 1  # standard deviation of oscillator frequencies
    dt = 0.0001  # KM evolution time per iteration

    def __init__(self, params={}):
        self.set_params(params)
        self.construct_ls()
        self.populate_with_oscillators()
        self.make_evolution_object()

    def set_params(self, params):
        for k in params:
            val = int(params[k]) if int(params[k]) == params[k] else params[k]
            setattr(self, k, val)

    def construct_ls(self):
        if self.net_type == 'rand_reg':
            net = NG.random_regular(self.n, self.d).net
        elif self.net_type == 'er':
            net = NG.er(self.n, self.q).net
        elif self.net_type == 'sf':
            net = NG.my_scale_free(self.n, self.gamma).net
        elif self.net_type == 'sw':
            net = NG.small_world(self.n, 2*self.K, self.q).net

        self.ls = Landscape(net)
        if self.ls_type == 'untargeted':
            self.ls.add_kms_by_kk_pp([self.k1, self.k2], [self.p, 1-self.p])
        elif self.ls_type == 'targeted':
            self.ls.add_kms_targeted(self.k1, self.k2, self.p)

    def populate_with_oscillators(self):
        oo = Oscillator.sample_normal_freq(self.N, self.w0, self.dw)
        oo[0].color = 'red'
        self.ls.populate_kms_randomly(oo)

    def make_evolution_object(self):
        self.ev = Evolution(self.ls, self.dt)
        self.ev.frames_per_iter = 1

    def run_till_relaxation(self, **args):
        self.ev.run_till_relaxation(**args)

    def get_order_param(self):
        return self.ev.rr[-1]

    def get_all_properties(self):
        props = {'net.typ.': self.net_type, 'ls.typ.': self.ls_type}
        if self.net_type == 'rand_reg':
            props.update({'n': self.n, 'd': self.d})
        elif self.net_type == 'er':
            props.update({'n': self.n, 'q': self.q})
        elif self.net_type == 'sf':
            props.update({'n': self.n, 'gamma': self.gamma})
        elif self.net_type == 'sw':
            props.update({'n': self.n, 'K': self.K, 'q': self.q})

        if self.ls_type == 'untargeted' or self.ls_type == 'targeted':
            props.update({'p': self.p, 'k1': self.k1, 'k2': self.k2})

        props.update({'N': self.N, 'dw': self.dw, 'dt': self.dt})
        return props

    def __str__(self):
        params = self.get_all_properties()
        # for key in params:
        #     if key=='dw': key='dw'
        name = ' '.join(([f'{k}={v}' for k, v in params.items()]))
        return 'RWO '+name


def f(param, save=False, verbose=False):
    rrwo = RWO_wrapper(param)
    if verbose:
        print(f' {rrwo}\n')
        sys.stdout.flush()
    rrwo.run_till_relaxation(verbose=verbose)
    return rrwo.get_order_param()


def construct_WH(params, copies, name_prefix=''):
    print('WorkHorse construction started')
    wh = WorkHorse(f=f, params=params, iters=copies,
                   parallel=running_on_cluster, job=job,
                   num_jobs=num_jobs, min_job=min_job)
    wh.f_out_num = 1
    wh.first_in_its_kind = job == 0
    wh.ylabels = ['r']

    # set name
    print('guineapig construction started')
    guineapig = RWO_wrapper(params[0])
    properties = guineapig.get_all_properties()
    for key in params[0]:
        properties[key] = 'var.'
    wh.name = ' '.join(([f'{k}={v}' for k, v in properties.items()]))
    wh.name = name_prefix+wh.name
    return wh


def run_WH(wh):
    print('started computing')
    wh.compute_data()
    print('done computing')
    if running_on_cluster:
        print('saving')
        wh.save()
    return wh


def plot_wh_2d(wh, fig=None, flip_xy=False):
    var_names = list(wh.pp[0].keys())
    var_names.sort(reverse=flip_xy)
    x_name, y_name = var_names
    xx = []
    yy = []
    zz = []
    z_err = []
    if not hasattr(wh, 'means'):
        wh.compute_means_and_errors()
    for params, z, sd in zip(wh.pp, wh.means, wh.errors):
        xx.append(params[x_name])
        yy.append(params[y_name])
        zz.append(z)
        z_err.append(sd)

    if fig is None:
        fig = plt.figure()

    zmin = 0
    zmax = 1
    plt.figure(fig)
    data_plot.contour_plot(xx, yy, zz,
                           lvls=np.linspace(zmin*1.001, zmax*1.001, 100),
                           extend_up_down=[zmax < 1, zmin > 0],
                           ticks=[zmin, (zmin+zmax)/2, zmax],
                           labels=[x_name, y_name, 'r'],
                           colors='summer')


def get_pc_exp(wh, x, x_name, plot=False, copy=None):
    # for a given k, pick out data for r vs p from wh, and fit
    ppc = []
    for copy in range(len(wh.data[-1])) if copy == None else [copy]:
        pp = set()
        for params in wh.pp:
            if params[x_name] == x:
                pp.update({params['p']})
        pp = list(pp)
        pp.sort()
        rr = np.array([0.]*len(pp))
        # outliers = []
        for i, params in enumerate(wh.pp):
            if x == params[x_name]:
                index = np.where(pp == params['p'])[0][0]
                if copy == -1:
                    r = wh.means[i]
                else:
                    r = wh.data[i][copy]
                rr[index] = r
                # if np.abs(r-wh.means[i])>0.5:
                #     outliers.append(index)

        # remove outliers
        # rr = np.delete(rr, outliers)
        # pp = np.delete(pp, outliers)
        # # for i in sorted(outliers, reverse=True):
        # #     del rr = np.delete(rr)
        # #     del pp[i]

        # r_approx = lambda p, pc, a: \
        #     ((1-p/pc) + (p/pc)*np.maximum(pc-p,0)**a) * np.heaviside(pc-p, 0)
        def r_approx(p, pc, a): return \
            2/np.pi*np.arctan(a*(pc-p)) * np.heaviside(pc-p, 0)
        pc, a = sp.optimize.curve_fit(r_approx, pp, rr,
                                      p0=[np.mean(pp)+.01*random(), 0.5]
                                      )[0]
        ppc.append(pc)

        if plot:
            plt.plot(pp, rr, '.')
            ppp = np.linspace(0, 1, 1000)
            # plt.plot(ppp, r_approx(ppp, pc, a))
            plt.xlim([min(pp), max(pp)])

    return np.mean(ppc), sp.stats.sem(ppc)


def get_kc_exp(wh, x, x_name, plot=False, copy=None):
    # for a given k, pick out data for r vs p from wh, and fit
    kkc = []
    for copy in range(len(wh.data[0])) if copy == None else [copy]:
        kk = set()
        for params in wh.pp:
            if params[x_name] == x:
                kk.update({params['k1']})
        kk = list(kk)
        kk.sort()
        rr = np.array([0.]*len(kk))
        for i, params in enumerate(wh.pp):
            if x == params[x_name]:
                index = np.where(kk == params['k1'])[0][0]
                if copy == -1:
                    r = wh.means[i]
                else:
                    r = wh.data[i][copy]
                rr[index] = r
        def r_approx(k, kc, a): return \
            2/np.pi*np.arctan(a*(k-kc)) * np.heaviside(k-kc, 0)
        try:
            kc, a = sp.optimize.curve_fit(r_approx, kk, rr,
                                          p0=[np.mean(kk)+.01*random(), 1]
                                          )[0]
        except:
            print(f'error in fitting {x_name}={x}, copy={copy}')
            kc, a = 0, 0
        kkc.append(kc)

        if plot:
            plt.plot(kk, rr, 'o-')
            kkk = np.linspace(min(kk), max(kk), 1000)
            plt.plot(kkk, r_approx(kkk, kc, a))
            plt.xlim([min(kk), max(kk)])

    return np.mean(kkc), sp.stats.sem(kkc)


def plot_pc_vs_x_exp(wh, x_name, **args):
    xx = set()
    for params in wh.pp:
        xx.update({params[x_name]})
    xx = list(xx)
    xx.sort()
    yy = []
    err = []
    for x in xx:
        y, er = get_pc_exp(wh, x, x_name)
        yy.append(y)
        err.append(er)
    if 'label' not in args:
        args['label'] = 'exp.'
    plt.errorbar(xx, yy, err, fmt='o', markersize=4, capsize=4, **args)
    plt.ylim(0, 1)


def plot_kc_vs_x_exp(wh, x_name):
    xx = set()
    for params in wh.pp:
        xx.update({params[x_name]})
    xx = list(xx)
    xx.sort()
    yy = []
    err = []
    for x in xx:
        y, er = get_kc_exp(wh, x, x_name)
        yy.append(y)
        err.append(er)
    plt.errorbar(xx, yy, err, fmt='o', markersize=4, capsize=4, label='exp.')


def pc_theo(x, name):
    guineapig = RWO_wrapper()
    prms = guineapig.get_all_properties()
    prms[name] = x
    if prms['net.typ.'] == 'rand_reg' and prms['ls.typ.'] == 'untargeted':
        n, k1, k2 = prms['n'], prms['k1'], prms['k2']
        dw, N = prms['dw'], prms['N']
        return (2*n*np.sqrt(2*np.pi)*dw/(np.pi*N) -
                k2) / (k1-k2)

    if prms['net.typ.'] == 'er' and prms['ls.typ.'] == 'untargeted':
        ratio = prms['q']*(prms['n']-1)/(1-prms['q']+prms['q']*(prms['n']-1))
        return (2*prms['n']*np.sqrt(2*np.pi)*prms['dw']/(np.pi*prms['N'])*ratio -
                prms['k2']) / (prms['k1']-prms['k2'])

    if prms['net.typ.'] == 'sw' and prms['ls.typ.'] == 'untargeted':
        n, q, k1, k2 = prms['n'], prms['q'], prms['k1'], prms['k2']
        dw, K, N = prms['dw'], prms['K'], prms['N']

        def Pr(d): return np.exp(-q*K) * sum([
            sp.special.binom(K, m)*(1-q)**m * q**(K-m) * (K*q)**(d-K-m)
            / sp.special.factorial(d-K-m)
            for m in range(min(d-K, K)+1)
        ])
        d_avg = sum([d*Pr(d) for d in range(K, n)])
        d2_avg = sum([d**2*Pr(d) for d in range(K, n)])
        c = d_avg**2/d2_avg
        return (k2 - c*np.sqrt(8/np.pi)*n/N*dw) / (k2-k1)

    if prms['net.typ.'] == 'sf' and prms['ls.typ.'] == 'untargeted':
        def z(x): return sp.special.zeta(x)
        c = z(prms['gamma']-1)**2/z(prms['gamma'])/z(prms['gamma']-2) \
            * np.heaviside(prms['gamma']-3, 0)
        c = np.nan_to_num(c)
        return (prms['k2'] -
                c*np.sqrt(8/np.pi)*prms['n']/prms['N']*prms['dw']
                ) / (prms['k2']-prms['k1'])


def kc_theo(x, name):
    guineapig = RWO_wrapper()
    prms = guineapig.get_all_properties()
    prms[name] = x

    if prms['net.typ.'] == 'er' and prms['ls.typ.'] == 'targeted':
        n, q, p, k2, N = prms['n'], prms['q'], prms['p'], prms['k2'], prms['N']
        dw = prms['dw']
        dc = (n-1)*q + np.sqrt(2*(n-1)*(1-q)*q)*sp.special.erfinv(1-2*p)
        d_avg = (n-1)*q
        sigma = np.sqrt((n-1)*(1-q)*q)
        def pr(d): return np.exp(-(d-d_avg)**2/2/sigma**2)/np.sqrt(2*np.pi)/sigma
        c1 = sp.integrate.quad(lambda d: d**2*pr(d), dc, n)[0]
        c2 = sp.integrate.quad(lambda d: d**2*pr(d), 0, dc)[0]
        return -1/c1 * (k2*c2 - np.sqrt(8/np.pi)*n/N*dw*d_avg**2)

    if prms['net.typ.'] == 'sw' and prms['ls.typ.'] == 'targeted':
        n, q, p, k2 = prms['n'], prms['q'], prms['p'], prms['k2']
        dw, K, N = prms['dw'], prms['K'], prms['N']

        def Pr(d): return np.exp(-q*K) * sum([
            sp.special.binom(K, m)*(1-q)**m * q**(K-m) * (K*q)**(d-K-m)
            / sp.special.factorial(d-K-m)
            for m in range(min(d-K, K)+1)
        ])
        d_avg = sum([d*Pr(d) for d in range(K, n)])
        d2_avg = sum([d**2*Pr(d) for d in range(K, n)])

        def float_sum(f, i_min, i_max=n+1):
            # print(i_min, np.ceil(i_min) - i_min, f(int(i_min)))
            return sum([f(i) for i in range(int(np.ceil(i_min)), i_max)]) + \
                (np.ceil(i_min) - i_min)*f(int(i_min))
        dc = sp.optimize.fsolve(lambda d: p-float_sum(Pr, d), d_avg)[0]
        c1 = float_sum(lambda d: d**2*Pr(d), dc)
        c2 = d2_avg-c1
        return - 1/c1*(c2*k2 - d_avg**2*np.sqrt(8/np.pi)*n/N*dw)

    if prms['net.typ.'] == 'sf' and prms['ls.typ.'] == 'targeted':
        n, gamma, p, k2 = prms['n'], prms['gamma'], prms['p'], prms['k2']
        dw, N = prms['dw'], prms['N']
        if gamma <= 3:
            return 0
        zeta = sp.special.zeta
        dc = sp.optimize.fsolve(
            lambda dc: p-zeta(gamma, dc)/zeta(gamma),
            1)[0]
        c1 = zeta(gamma-2)/zeta(gamma-2, dc) - 1
        c2 = zeta(gamma-1)**2/zeta(gamma)/zeta(gamma-2, dc)
        return -(c1*k2-c2*np.sqrt(8/np.pi)*n/N*dw)


def plot_pc_theo_vs_x(x0, x1, x_name):
    xx = np.linspace(x0, x1, 1000)
    yy = pc_theo(xx, x_name)
    if not hasattr(yy, '__iter__'):
        yy = [yy]*len(xx)
    # print(xx, yy)
    plt.plot(xx, yy, label='theo.')


def plot_kc_theo_vs_x(x_min, x_max, x_name):
    xx = np.linspace(x_min, x_max, 100)
    yy = [kc_theo(x, x_name) for x in xx]
    plt.plot(xx, yy, label='theo.')


print('python file launched')


x_name = 'gamma'
x_min = 2.5
x_max = 4
x_num = 11

y_name = 'p'
del_y = 1
y_num = 100

params = []
for x in np.linspace(x_min, x_max, x_num):
    if y_name == 'p':
        yc = pc_theo(x, x_name)
        del_y = min(del_y, 1-yc, yc)
    elif y_name == 'k1':
        yc = kc_theo(x, x_name)

    for y in np.linspace(yc-del_y/2, yc+del_y/2, y_num):
        params.append({y_name: y, x_name: x})


if running_on_cluster:
    wh = construct_WH(params, copies=10, name_prefix='')
    run_WH(wh)

else:
    # wh_name = 'WH net.typ.=sf ls.typ.=untargeted n=1000 gamma=var. p=var. k1=-1 k2=1 N=5000 dw=1 dt=0.001'
    # wh_name = 'WH net.typ.=sf ls.typ.=untargeted n=100 gamma=var. p=var. k1=-1 k2=1 N=500 dw=1 dt=0.001'
    # wh_name = 'WH net.typ.=sf ls.typ.=untargeted n=100 gamma=var. p=var. k1=-1 k2=1 N=500 dw=1 dt=0.0001'
    # wh_name = 'WH net.typ.=sf ls.typ.=untargeted n=1000 gamma=var. p=var. k1=-1 k2=1 N=5000 dw=1 dt=0.0001'
    # wh_name = 'WH net.typ.=sf ls.typ.=untargeted n=100 gamma=var. p=var. k1=-1 k2=1 N=500 dw=1 dt=0.0001'
    wh_name = "WH net.typ.=sf ls.typ.=untargeted n=2000 gamma=var. p=var. k1=-1 k2=1 N=10000 dw=1 dt=0.0001"

    wh = WorkHorse.load(wh_name)
    # plot_pc_vs_x_exp(wh, x_name, label=wh_name)
    if y_name == 'p':
        # plot_wh_2d(wh, flip_xy=False)
        plot_pc_theo_vs_x(x_min, x_max, x_name)
        plot_pc_vs_x_exp(wh, x_name)
    elif y_name == 'k1':
        # plot_wh_2d(wh, flip_xy=False)
        plot_kc_theo_vs_x(x_min, x_max, x_name)
        plot_kc_vs_x_exp(wh, x_name)

    plt.title(wh, wrap=True)
    plt.xlabel(x_name)
    plt.ylabel(y_name)
    plt.legend()

    # plt.figure()
    # for i in range(10):
    #     # plt.clf()
    #     get_pc_exp(wh, 2.8, x_name, plot=True, copy=i)
    #     plt.pause(2)

    # get_kc_exp(wh, 2.5, 'gamma', plot=True, copy=-1)
    # ng = NG.scale_free_static_model(1000,4)
    # ng.plot_degree_distr_log(line=4)
