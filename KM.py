import numpy as np
from scipy.integrate import RK45
import matplotlib.pyplot as plt


n = 6
dw = 0.
w0 = 2
k = 10
T = 100

phi = np.random.rand(n)*2*np.pi
w = np.random.normal(w0, dw, n)

plot_every = 1


def plot():
    plt.gca().cla()
    plt.scatter(np.cos(phiphi[-1]), np.sin(phiphi[-1]))
    plt.xlim([-1.2, 1.2])
    plt.ylim([-1.2, 1.2])
    plt.title(f't={tt[-1]:.2f}')
    plt.gca().axis('off')
    plt.gca().set_aspect('equal')
    plt.pause(0.01)


def dphidt(t, phi):
    ans = np.zeros(n)
    for i in range(n):
        ans[i] += w[i]
        r1 = np.array([np.cos(phi[i]), np.sin(phi[i])])
        tang = np.array([np.sin(phi[i]), -np.cos(phi[i])])
        for j in range(n):
            if i == j:
                continue
            r2 = np.array([np.cos(phi[j]), np.sin(phi[j])])
            f = k/n * (r2-r1)  # / np.linalg.norm(r2-r1)**2
            ans[i] = np.dot(f, tang)
        print(ans)
    return ans


# Integrate the Kuramoto model
sol = RK45(dphidt, 0, phi, T)


# Lists to store the solution
tt = [0]
phiphi = [phi]

while sol.status == 'running':
    sol.step()
    tt.append(sol.t)
    phiphi.append(sol.y)
    if len(tt) % plot_every == 0:
        plot()
