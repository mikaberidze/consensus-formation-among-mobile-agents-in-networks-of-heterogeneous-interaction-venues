#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 14 23:34:03 2021

@author: guga
"""

from tqdm import tqdm
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import matplotlib.patches as mpatches
from PIL import Image, ImageDraw
import PIL
from matplotlib.figure import Figure
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import numpy as np
import scipy
import random
import itertools
import networkx as nx
import random as rnd


class LandscapePlot:
    node_r = 0.25
    phase_ball_r = 0.03
    bar_hight = 0.2  # fraction of canvas
    lw = 1
    edge_color = "lightgray"
    node_color = "white"
    phase_color = 'lightgreen'
    node_fill = True

    def __init__(self, ls):
        self.ls = ls
        self.net = ls.net
        self.edge_colors = {e: LandscapePlot.edge_color for e in self.net.edges()}
        self.N = len(self.net)
        self.borders = [[0, 1.2*self.N**.5], [0, 1.2*self.N**.5]]
        self.layout = {}
        self.random_layout()
        self.plot_couplings = True
        self.title = None
        self.save_frames = True
        self.frames = []  # frames to export animation later
        self.bars = {}
        self.bar_colors = {}
        self.draggable_nodes = True

    def color_edges(self, edges, color):
        for e in edges:
            self.edge_colors[e] = color

    def reset_edge_colors(self):
        self.color_edges(self.net.edges(), LandscapePlot.edge_color)

    def plot_2d_transition(self, traj, iters=20):
        for it in range(iters+1):
            self.plot_carcass_2d()
            self.add_phase_satelites_2d_transient(traj, it/iters)
            if self.title is not None:
                self.display_lable_2d(self.title)
            self.include_order_param()
            self.plot_bars_2d()
            plt.pause(0.01)
            if self.save_frames:
                self.pull_canvas_frame_2d()

    def plot_2d(self):
        self.plot_carcass_2d()
        self.add_phase_satelites_2d()
        if self.title is not None:
            self.display_lable_2d(self.title)
        self.include_order_param()
        self.plot_bars_2d()
        plt.pause(0.01)
        if self.save_frames:
            self.pull_canvas_frame_2d()

    def display_lable_2d(self, label):
        ax = self.get_2d_axis()
        ax.set_title(label)

    def add_phase_satelites_2d(self):
        ax = self.get_2d_axis()
        for nd in self.net.nodes():
            for o1 in self.ls.kms[nd].oscillators:
                for o2 in self.ls.kms[nd].oscillators:
                    phi1 = o1.phase
                    x1, y1 = self.layout[nd]
                    x1 += LandscapePlot.node_r*np.cos(phi1)
                    y1 += LandscapePlot.node_r*np.sin(phi1)

                    phi2 = o2.phase
                    x2, y2 = self.layout[nd]
                    x2 += LandscapePlot.node_r*np.cos(phi2)
                    y2 += LandscapePlot.node_r*np.sin(phi2)

                    ax.plot([x1, x2], [y1, y2], '-', color='grey',
                            linewidth=0.05, zorder=19)

        for nd in self.net.nodes():
            for o in self.ls.kms[nd].oscillators:
                phi = o.phase
                x, y = self.layout[nd]
                x += LandscapePlot.node_r*np.cos(phi)
                y += LandscapePlot.node_r*np.sin(phi)
                oscil_col = LandscapePlot.phase_color if o.color == None else o.color
                cir1 = plt.Circle((x, y), LandscapePlot.phase_ball_r,
                                  color=oscil_col, zorder=20)
                cir2 = plt.Circle((x, y), LandscapePlot.phase_ball_r,
                                  color='black', fill=False,
                                  lw=LandscapePlot.lw, zorder=20)
                ax.add_patch(cir1)
                ax.add_patch(cir2)
                # ax.(x, y, 0, c="white", edgecolors='black', s=8)

    def add_phase_satelites_2d_transient(self, traj, t):
        ax = self.get_2d_axis()
        for (o, (nd1, nd2)) in traj.items():
            phi = o.phase
            x, y = np.array(self.layout[nd1])*(1-t) \
                + np.array(self.layout[nd2])*t
            rad_coef = (1-0.85*np.sin(t*np.pi))
            x += LandscapePlot.node_r*np.cos(phi)*rad_coef
            y += LandscapePlot.node_r*np.sin(phi)*rad_coef
            oscil_col = LandscapePlot.phase_color if o.color == None else o.color
            cir1 = plt.Circle((x, y), LandscapePlot.phase_ball_r,
                              color=oscil_col)
            cir2 = plt.Circle((x, y), LandscapePlot.phase_ball_r,
                              color='black', fill=False, lw=LandscapePlot.lw)
            ax.add_patch(cir1)
            ax.add_patch(cir2)
            # ax.(x, y, 0, c="white", edgecolors='black', s=8)

    def plot_bars_2d(self):  # TODO!
        ax = self.get_2d_axis()
        ((x0, x1), (y0, y1)) = self.borders
        hight = (y1-y0)/5
        width = hight/5
        offset = width*1.1
        for lbl in self.bars:
            val = self.bars[lbl]
            color = self.bar_colors[lbl] if lbl in self.bar_colors \
                else "grey"
            a = x1 - offset
            b = y1 - hight*1.1
            h = hight*val
            rect1 = mpatches.Rectangle((a, b), width, h,
                                       fill=True,
                                       color=color,
                                       linewidth=1)
            rect2 = mpatches.Rectangle((a, b), width, hight,
                                       fill=False,
                                       color="grey",
                                       linewidth=1)
            plt.text(a+width/2, b-hight/10, lbl, horizontalalignment='center',
                     verticalalignment="top")
            ax.add_patch(rect1)
            ax.add_patch(rect2)
            offset += width*2

    def include_order_param(self):
        ''' r to be represented as a bar '''
        self.bars['r'] = self.ls.get_order_param()
        self.bar_colors['r'] = 'blue'

    def plot_carcass_2d(self):
        ax = self.get_2d_axis()
        ax.clear()
        for edge in self.net.edges():
            self.draw_edge_2d(ax, edge, color=self.edge_colors[edge])
        plt.show()

        cirs = [[], []]
        for node in self.net.nodes():
            cir = self.draw_node_2d(ax, node)
            if self.draggable_nodes:
                cirs[0].append(node)
                cirs[1].append(cir)
        if self.draggable_nodes and not hasattr(self, 'draggable_circles'):
            self.draggable_circles = DraggableCircles(*cirs, self)
        plt.show()
        ax.axis("off")
        ax.set_aspect('equal')
        if self.bars:  # make sure bars are visible
            ax.set_xlim([ax.get_xlim()[0], max(ax.get_xlim()[1], self.borders[0][1])])
            ax.set_ylim([ax.get_ylim()[0], max(ax.get_ylim()[1], self.borders[1][1])])

    def draw_edge_2d(self, ax, edge, color="lightgray", linewidth=2):
        v0 = np.array(self.layout[edge[0]])
        v1 = np.array(self.layout[edge[1]])
        e = (v0-v1)
        e /= np.linalg.norm(e)
        v0 -= e*LandscapePlot.node_r
        v1 += e*LandscapePlot.node_r

        ax.plot([v0[0], v1[0]], [v0[1], v1[1]], '-', color=color,
                linewidth=linewidth, zorder=-1000)

    def draw_node_2d(self, ax, node):
        x, y = self.layout[node]

        if LandscapePlot.node_fill:
            cir1 = plt.Circle((x, y), LandscapePlot.node_r,
                              color=LandscapePlot.node_color)
            ax.add_patch(cir1)
        cir2 = plt.Circle((x, y), LandscapePlot.node_r, color='k',
                          lw=LandscapePlot.lw, fill=False)
        ax.add_patch(cir2)
        # ax.plot(x, y, 'o', color='k')
        if self.plot_couplings:
            plt.text(x, y, f'k={self.ls.kms[node].k}',
                     ha='center', va='center', size=30*self.node_r,
                     color='b' if self.ls.kms[node].k > 0 else 'r',
                     weight='bold')
        return cir2

    def get_2d_axis(self):
        if hasattr(self, "ax_2d"):
            return self.ax_2d
        self.ax_2d = plt.subplot()
        return self.ax_2d

    def random_layout(self):
        ''' randomly scatter the nodes in the canvas '''
        for node in self.net.nodes():
            xmin, xmax = self.borders[0]
            ymin, ymax = self.borders[1]
            self.layout[node] = [xmin+(xmax-xmin)*rnd.random(),
                                 ymin+(ymax-ymin)*rnd.random()]
        return self.layout

    def force_evolve_layout(self, t=10, dt=0.1, atol=0.1,
                            animate=False, verbose=True):
        ''' implements forced balance representation of the graph '''
        k = 1  # Hooks law
        c = 1  # Coulombs law
        n = 5  # wall reaction force

        nodes = list(self.layout)
        node_index_map = {nd: i for (i, nd) in enumerate(nodes)}
        xy = np.array(list(self.layout.values()))
        shapexy = xy.shape
        sizexy = shapexy[0]*shapexy[1]

        def neighbors(i):
            nd = nodes[i]
            ngbrs = self.net.neighbors(nd)
            return [node_index_map[ngbr] for ngbr in ngbrs]

        def dist(xy0, xy1):
            return ((xy0[0]-xy1[0])**2+(xy0[1]-xy1[1])**2)**.5

        def force(t, xy_flat):
            xy = np.reshape(xy_flat, shapexy)
            forces = [np.array([0., 0.]) for i in range(len(nodes))]
            for i in range(len(nodes)):
                jj = neighbors(i)

                # attraction
                for j in jj:
                    forces[i] += k*(xy[j]-xy[i])

                # repulsion
                for j in range(len(nodes)):
                    if i != j:
                        forces[i] -= c*(xy[j]-xy[i])/dist(xy[j], xy[i])**3

                # borders
                x, y = xy[i]
                xmin, xmax = self.borders[0]
                ymin, ymax = self.borders[1]
                r = LandscapePlot.node_r
                N = np.array([0., 0.])
                # square borders
                # if x-3*r<xmin: N[0] += n*(xmin-x+3*r)**2
                # if x+3*r>xmax: N[0] -= n*(x-xmax+3*r)**2
                # if y-3*r<ymin: N[1] += n*(ymin-y+3*r)**2
                # if y+3*r>ymax: N[1] -= n*(y-ymax+3*r)**2
                # circular
                center = np.array([xmin+xmax, ymin+ymax])/2
                R = (xmax-xmin)/2
                if np.linalg.norm(xy[i]-center)+3*r > R:
                    N += n*(center-xy[i])/np.linalg.norm(center-xy[i]) *\
                        (np.linalg.norm(xy[i]-center)+3*r-R)**2

                # away from corner
                corner = [self.borders[0][1], self.borders[1][1]]
                R = (ymax - ymin)*LandscapePlot.bar_hight*2
                if self.bars and \
                        np.linalg.norm(xy[i]-corner) < R:
                    N += n*(xy[i]-corner)/np.linalg.norm(xy[i]-corner) *\
                        (R-np.linalg.norm(xy[i]-corner))**2

                forces[i] += N

            return np.reshape(forces, sizexy)

        rng = range(int(t/dt))
        for i in tqdm(rng) if verbose else rng:
            xy = np.array(list(self.layout.values()))
            sol = solve_ivp(force, [0, dt], np.reshape(xy, sizexy), atol=atol)
            xy_final = np.reshape(np.transpose(sol.y)[-1], shapexy)
            self.layout = {nodes[i]: list(xy_final[i])
                           for i in range(len(nodes))}

            if animate:
                self.get_2d_axis().clear()
                self.plot_carcass_2d()
                plt.pause(0.01)

    def interactive_force_layout(self, t_0=1):
        t = t_0
        atol = 0.2
        while True:
            self.force_evolve_layout(t, animate=True, atol=atol)
            value = None
            while value is None:
                value = input(f"Should we continue balancing it? (current tolerance:{atol}) \n"
                              "int - continue \n"
                              "enter - finish \n"
                              "d - double tolerance \n"
                              "h - half tolerance \n")
                if value == "d":
                    atol *= 2
                    value = None
                if value == "h":
                    atol /= 2
                    value = None
                if value != "" and not str(value).isdigit():
                    value = None
            if value == "":
                break
            else:
                t = int(value)

    def pull_canvas_frame_2d(self):
        fig = self.get_2d_axis().get_figure()
        canvas = FigureCanvas(fig)
        canvas.draw()
        image = PIL.Image.frombytes('RGB',
                                    fig.canvas.get_width_height(), fig.canvas.tostring_rgb())

        self.frames.append(image)

    def export_animation(self, name="out"):
        self.frames[0].save(f"Out/{name}.gif", save_all=True,
                            append_images=self.frames[1:], loop=0, duration=10)


class DraggableCircles:
    def __init__(self, node_ids, circles, lp):
        self.circles = circles
        self.node_ids = node_ids
        self.press = None
        self.lp = lp
        self.active_id = -1
        self.connect()
        self.paused = False

    def connect(self):
        'connect to all the events we need'
        self.cidpress = self.circles[0].figure.canvas.mpl_connect(
            'button_press_event', self.on_press)
        self.cidrelease = self.circles[0].figure.canvas.mpl_connect(
            'button_release_event', self.on_release)
        self.cidmotion = self.circles[0].figure.canvas.mpl_connect(
            'motion_notify_event', self.on_motion)

    def on_press(self, event):
        'on button press we will see if the mouse is over us and store some data'
        if event.inaxes != self.circles[0].axes or self.paused:
            return
        for circle, node in zip(self.circles, self.node_ids):
            contains, attrd = circle.contains(event)
            if contains:
                self.press = (circle.center), event.xdata, event.ydata
                self.active_id = node
                return

    def on_motion(self, event):
        'on motion we will move the rect if the mouse is over us'
        if self.press is None or self.paused:
            return
        cir = self.circles[self.active_id]
        if event.inaxes != cir.axes:
            return
        cir.center, xpress, ypress = self.press
        dx = event.xdata - xpress
        dy = event.ydata - ypress
        x = cir.center[0]+dx
        y = cir.center[1]+dy
        cir.center = (x, y)
        self.lp.layout[self.active_id] = (x, y)
        self.lp.plot_carcass_2d()
        plt.pause(0.01)

    def on_release(self, event):
        'on release we reset the press data'
        if not self.press == None and not self.paused and event.xdata is not None:
            self.circles[self.active_id].center, xpress, ypress = self.press
            self.press = None
            self.paused = True
            dx = event.xdata - xpress
            dy = event.ydata - ypress
            x = self.circles[self.active_id].center[0]+dx
            y = self.circles[self.active_id].center[1]+dy
            self.circles[self.active_id].center = (x, y)
            self.lp.layout[self.active_id] = (x, y)
            self.lp.force_evolve_layout(1, animate=True, verbose=False)
            self.collect_new_locs()
            self.paused = False

    def collect_new_locs(self):
        for nd, cir in zip(self.node_ids, self.circles):
            cir.center = self.lp.layout[nd]

    def disconnect(self):
        'disconnect all the stored connection ids'
        self.circles[0].figure.canvas.mpl_disconnect(self.cidpress)
        self.circles[0].figure.canvas.mpl_disconnect(self.cidrelease)
        self.circles[0].figure.canvas.mpl_disconnect(self.cidmotion)
