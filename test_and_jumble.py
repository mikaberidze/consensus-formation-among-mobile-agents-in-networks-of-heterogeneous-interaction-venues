#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  2 12:06:30 2023

@author: guga
"""

from mod_LP import LandscapePlot
from mod_NG import NetworkGenerator
from mod_RWO import Landscape, Oscillator


ng = NetworkGenerator.random(10,15)

ls = Landscape(ng.net)
ls.add_kms_targeted(-1, 1, 0.21)

oo = Oscillator.sample_normal_freq(10,1,1)
ls.populate_kms_randomly(oo)

lp = LandscapePlot(ls)
lp.force_evolve_layout()

lp.plot_2d()

